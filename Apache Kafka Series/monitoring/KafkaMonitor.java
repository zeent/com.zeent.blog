/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class KafkaMonitor {
    private static int JMX_PORT = 9010;
    private static String JMX_HOST = "localhost";
    static final String[] excludedAttributes_VALUES = new String[]{"EventType", "RateUnit", "LatencyUnit", "FiveMinuteRate", "FifteenMinuteRate", "StdDev", "75thPercentile", "95thPercentile", "50thPercentile", "98thPercentile", "99thPercentile", "999thPercentile"};
    static Set<String> excludedAttributes = new HashSet<String>(Arrays.asList(excludedAttributes_VALUES));
    static final String[] replaceSet_VALUES = new String[]{":name=", ",type=", ",request="};
    static Set<String> replaceSet = new HashSet<String>(Arrays.asList(replaceSet_VALUES));
    static final String[] objectNames_VALUES = new String[]{	"kafka.server:type=ReplicaManager,name=UnderReplicatedPartitions",
							   	"kafka.controller:type=KafkaController,name=OfflinePartitionsCount",
								"kafka.controller:type=KafkaController,name=ActiveControllerCount",
								"kafka.server:type=BrokerTopicMetrics,name=MessagesInPerSec",
								"kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec",
								"kafka.server:type=BrokerTopicMetrics,name=BytesOutPerSec",
								"kafka.network:name=RequestsPerSec,request=Produce,type=RequestMetrics",
								"kafka.network:name=RequestsPerSec,request=FetchConsumer,type=RequestMetrics", 									"kafka.network:name=RequestsPerSec,request=FetchFollower,type=RequestMetrics", 									"kafka.network:name=TotalTimeMs,request=FetchFollower,type=RequestMetrics", 									"kafka.network:name=TotalTimeMs,request=FetchConsumer,type=RequestMetrics", 									"kafka.network:name=TotalTimeMs,request=Produce,type=RequestMetrics",
								"kafka.controller:name=LeaderElectionRateAndTimeMs,type=ControllerStats",
								"kafka.server:name=PartitionCount,type=ReplicaManager",
								"kafka.server:name=IsrShrinksPerSec,type=ReplicaManager",
								"kafka.server:name=IsrExpandsPerSec,type=ReplicaManager",
								"kafka.network:name=NetworkProcessorAvgIdlePercent,type=SocketServer",
								"kafka.network:name=NetworkProcessorAvgIdlePercent,type=SocketServer", 									"kafka.server:name=RequestHandlerAvgIdlePercent,type=KafkaRequestHandlerPool",
								"kafka.network:type=RequestMetrics,name=RequestsPerSec,request=Produce"

							};

    static Set<String> objectNames = new HashSet<String>(Arrays.asList(objectNames_VALUES));

    private static String getValues(String string, MBeanServerConnection mBeanServerConnection) {
        StringBuilder stringBuilder = new StringBuilder();
        String string2 = "";
        try {
            MBeanAttributeInfo[] arrmBeanAttributeInfo;
            ObjectName objectName = new ObjectName(string);
            MBeanInfo mBeanInfo = mBeanServerConnection.getMBeanInfo(objectName);
            for (MBeanAttributeInfo mBeanAttributeInfo : arrmBeanAttributeInfo = mBeanInfo.getAttributes()) {
                try {
                    /*if (excludedAttributes.contains(mBeanAttributeInfo.getName())) continue;*/
                    stringBuilder.append("\t<result>\n");
                    Object object = mBeanServerConnection.getAttribute(objectName, mBeanAttributeInfo.getName());
                    string2 = objectName.getCanonicalName() + '.' + mBeanAttributeInfo.getName();
                    for (String string3 : replaceSet) {
                        string2 = string2.replace(string3, ".");
                    }
                    stringBuilder.append("\t\t<channel>" + string2 + "</channel>\n");
                    stringBuilder.append("\t\t<showChart>1</showChart>\n");
                    stringBuilder.append("\t\t<showTable>1</showTable>\n");
                    stringBuilder.append("\t\t<float>1</float>\n");
                    stringBuilder.append("\t\t<value>" + Double.parseDouble(object.toString()) + "</value>\n");
                    stringBuilder.append("\t</result>\n");
                    continue;
                }
                catch (Exception var11_13) {
                    return "";
                }
            }
        }
        catch (Exception var4_5) {
            return "";
        }
        return stringBuilder.toString();
    }

    public static void main(String[] arrstring) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            JMXServiceURL jMXServiceURL = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + JMX_HOST + ":" + JMX_PORT + "/jmxrmi");
            JMXConnector jMXConnector = JMXConnectorFactory.connect(jMXServiceURL, null);
            MBeanServerConnection mBeanServerConnection = jMXConnector.getMBeanServerConnection();
            stringBuilder.append("<?xml version=\"1.0\" ?>\n");
            stringBuilder.append("<prtg>\n");
            stringBuilder.append("\t<result>\n");
            stringBuilder.append("\t\t<channel>kafka.server.Status.Online</channel>\n");
            stringBuilder.append("\t\t<showChart>1</showChart>\n");
            stringBuilder.append("\t\t<showTable>1</showTable>\n");
            stringBuilder.append("\t\t<float>1</float>\n");
            stringBuilder.append("\t\t<value>1</value>\n");
            stringBuilder.append("\t</result>\n");
            for (String string : objectNames) {
                stringBuilder.append(KafkaMonitor.getValues(string, mBeanServerConnection));
            }
            stringBuilder.append("</prtg>\n");
            System.out.println(stringBuilder.toString());
        }
        catch (Exception var2_3) {
            stringBuilder.append("<?xml version=\"1.0\" ?>\n");
            stringBuilder.append("<prtg>\n");
            stringBuilder.append("\t<result>\n");
            stringBuilder.append("\t\t<channel>kafka.server.Status.Online</channel>\n");
            stringBuilder.append("\t\t<showChart>1</showChart>\n");
            stringBuilder.append("\t\t<showTable>1</showTable>\n");
            stringBuilder.append("\t\t<float>1</float>\n");
            stringBuilder.append("\t\t<value>0</value>\n");
            stringBuilder.append("\t</result>\n");
            stringBuilder.append("</prtg>\n");
            System.out.println(stringBuilder.toString());
        }
    }
}
